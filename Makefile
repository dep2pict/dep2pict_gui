doc:
	@echo "make build: run python3 setup.py"
	@echo "make upload_test: upload on the test.pypi website"
	@echo "make upload_prod: upload on the prod pypi website"

build:
	rm -rf dist
	python3 setup.py sdist

upload_test:
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*
	@echo "INSTALL: pip install --index-url https://test.pypi.org/simple/ dep2pict-gui"

upload_prod:
	twine upload dist/*

purge:
	rm -rf dist
	rm -rf dep2pict_gui.egg-info